var operators = [];
var maxNumber = 15
var currentQuestion = 1
var numOfQuestions = 10
var answeredCorrect = 0
var answeredInCorrect = 0

var checkAnswer = () => {
    let submitBtn = document.getElementById("submitBtn")
    let answerField = document.getElementById("answer")
    if (submitBtn.innerText == "Submit") {
        let fNum = Number(document.getElementById("fNum").innerText);
        let sNum = Number(document.getElementById("sNum").innerText);
        let answerEntered = Number(document.getElementById("answer").value);
        let operator = document.getElementById("operator").innerText

        switch (operator) {
            case "+":
                if (answerEntered == fNum + sNum) {
                    markCardCorrect()
                } else {
                    markCardWrong()
                };
                break;

            case "-":
                if (answerEntered == fNum - sNum) {
                    markCardCorrect()
                } else {
                    markCardWrong()
                };
                break;
            case "*":
                if (answerEntered == fNum * sNum) {
                    markCardCorrect()
                } else {
                    markCardWrong()
                }
                break;
            default:
                break;
        }

        updateScoreBoard()
        if (currentQuestion > numOfQuestions) {
            submitBtn.innerText = "Start Over"
            answerField.disabled = true
        } else {
            submitBtn.innerText = "Next"
            answerField.disabled = true
        }
    } else if (submitBtn.innerText == "Start Over") {
        currentQuestion = 1
        document.getElementById("currentQuestion").innerText = currentQuestion

        answeredCorrect = 0
        answeredInCorrect = 0
        resetCardStyles()
    } else {
        resetCardStyles()
    }
}

var enable_disable_btn = () => {
    let answerInput = document.getElementById("answer")
    if (answerInput.value.length == 0) {
        document.getElementById("submitBtn").disabled = true
    } else {
        document.getElementById("submitBtn").disabled = false
    }
}

window.onload = (event) => {
    enable_disable_btn()
    generateRandomValues()
    document.getElementById("max-number").value = maxNumber
    document.getElementById("number-of-questions").value = numOfQuestions
    document.getElementById("currentQuestion").innerHTML = currentQuestion
    document.getElementById("numberOfQuestions").innerHTML = numOfQuestions
    updateScoreBoard()

    observer.observe(body, {
        childList: true, // observe direct children
    });
};

var markCardWrong = () => {
    document.querySelector(".card").classList.add("border-danger")
    document.querySelector(".card-body").classList.add("text-danger")
    document.getElementById("answer").style.color = "#dc3545"
    answeredInCorrect++
    currentQuestion++
}

var markCardCorrect = () => {
    document.querySelector(".card").classList.add("border-success")
    document.querySelector(".card-body").classList.add("text-success")
    document.getElementById("answer").style.color = "#28a745"
    answeredCorrect++
    currentQuestion++
}

var resetCardStyles = () => {
    let answerField = document.getElementById("answer")
    document.querySelector(".card").classList.remove("border-success")
    document.querySelector(".card-body").classList.remove("text-success")
    document.querySelector(".card").classList.remove("border-danger")
    document.querySelector(".card-body").classList.remove("text-danger")
    document.getElementById("answer").style.color = "#495057"
    document.getElementById("answer").value = ""
    document.getElementById("submitBtn").innerText = "Submit"
    answerField.disabled = false
    enable_disable_btn()
    generateRandomValues()
}

// min and max included 
var randomIntFromInterval = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min)
}

var generateRandomValues = () => {
    getCheckedOperators()
    let randomNum1 = randomIntFromInterval(0, maxNumber)
    let randomNum2 = randomIntFromInterval(0, maxNumber)
    let operator = operators[randomIntFromInterval(0, (operators.length - 1))]
    document.getElementById("operator").innerText = operator
    if (operator == "-" || operator == "/") {
        document.getElementById("fNum").innerText = Math.max(randomNum1, randomNum2)
        document.getElementById("sNum").innerText = Math.min(randomNum1, randomNum2)
    } else {
        document.getElementById("fNum").innerText = randomIntFromInterval(0, maxNumber)
        document.getElementById("sNum").innerText = randomIntFromInterval(0, maxNumber)
    }
}

var saveParametersChanges = () => {
    maxNumber = document.getElementById("max-number").value
    numOfQuestions = document.getElementById("number-of-questions").value
    document.getElementById("numberOfQuestions").innerText = numOfQuestions
    generateRandomValues()
}

var getCheckedOperators = () => {
    let inputs = document.querySelectorAll(".form-check-input")
    for (const checkbox of inputs) {
        console.log("checkbox value", checkbox.checked)
        if (checkbox.checked == true) {
            operators.push(checkbox.value)
            console.log("operators", operators)
        } else if (checkbox.checked == false && operators.indexOf(checkbox.value)>=0){
            console.log("test")
            operators.splice(operators.indexOf(checkbox.value), 1)
        }
    }
    console.log("operators", operators)
    operators = [... new Set(operators)]
}

var updateScoreBoard = () => {
    document.getElementById("correct").innerText = answeredCorrect
    document.getElementById("in-correct").innerText = answeredInCorrect
    document.getElementById("currentQuestion").innerText = currentQuestion
    document.getElementById("percentage").innerText = `${((answeredCorrect / numOfQuestions) * 100).toFixed(2)}%`
}

var clearNotes = () => {
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
}

function startup() {
    const el = document.getElementById('canvas');
    el.addEventListener('touchstart', handleStart);
    el.addEventListener('touchend', handleEnd);
    el.addEventListener('touchcancel', handleCancel);
    el.addEventListener('touchmove', handleMove);

    document.getElementById("canvas").height = window.innerHeight
    document.getElementById("canvas").width = window.innerWidth
    //log('Initialized.');
}

document.addEventListener("DOMContentLoaded", startup);

const ongoingTouches = [];

function handleStart(evt) {
    evt.preventDefault();
    // //log('touchstart.');
    const el = document.getElementById('canvas');
    const ctx = el.getContext('2d');
    const touches = evt.changedTouches;

    for (let i = 0; i < touches.length; i++) {
        ////log(`touchstart: ${i}.`);
        ongoingTouches.push(copyTouch(touches[i]));
        const color = colorForTouch(touches[i]);
        ////log(`color of touch with id ${touches[i].identifier} = ${color}`);
        ctx.beginPath();
        ctx.arc(touches[i].pageX, touches[i].pageY, 4, 0, 2 * Math.PI, false);  // a circle at the start
        ctx.fillStyle = color;
        ctx.fill();
    }
}

function handleMove(evt) {
    evt.preventDefault();
    const el = document.getElementById('canvas');
    const ctx = el.getContext('2d');
    const touches = evt.changedTouches;

    for (let i = 0; i < touches.length; i++) {
        const color = colorForTouch(touches[i]);
        const idx = ongoingTouchIndexById(touches[i].identifier);

        if (idx >= 0) {
            //log(`continuing touch ${idx}`);
            ctx.beginPath();
            //log(`ctx.moveTo( ${ongoingTouches[idx].pageX}, ${ongoingTouches[idx].pageY} );`);
            ctx.moveTo(ongoingTouches[idx].pageX, ongoingTouches[idx].pageY);
            //log(`ctx.lineTo( ${touches[i].pageX}, ${touches[i].pageY} );`);
            ctx.lineTo(touches[i].pageX, touches[i].pageY);
            ctx.lineWidth = 4;
            ctx.strokeStyle = color;
            ctx.stroke();

            ongoingTouches.splice(idx, 1, copyTouch(touches[i]));  // swap in the new touch record
        } else {
            //log('can\'t figure out which touch to continue');
        }
    }
}

function handleEnd(evt) {
    evt.preventDefault();
    //log("touchend");
    const el = document.getElementById('canvas');
    const ctx = el.getContext('2d');
    const touches = evt.changedTouches;

    for (let i = 0; i < touches.length; i++) {
        const color = colorForTouch(touches[i]);
        let idx = ongoingTouchIndexById(touches[i].identifier);

        if (idx >= 0) {
            ctx.lineWidth = 4;
            ctx.fillStyle = color;
            ctx.beginPath();
            ctx.moveTo(ongoingTouches[idx].pageX, ongoingTouches[idx].pageY);
            ctx.lineTo(touches[i].pageX, touches[i].pageY);
            ctx.fillRect(touches[i].pageX - 4, touches[i].pageY - 4, 8, 8);  // and a square at the end
            ongoingTouches.splice(idx, 1);  // remove it; we're done
        } else {
            //log('can\'t figure out which touch to end');
        }
    }
}

function handleCancel(evt) {
    evt.preventDefault();
    //log('touchcancel.');
    const touches = evt.changedTouches;

    for (let i = 0; i < touches.length; i++) {
        let idx = ongoingTouchIndexById(touches[i].identifier);
        ongoingTouches.splice(idx, 1);  // remove it; we're done
    }
}

function colorForTouch(touch) {
    let r = touch.identifier % 16;
    let g = Math.floor(touch.identifier / 3) % 16;
    let b = Math.floor(touch.identifier / 7) % 16;
    r = r.toString(16); // make it a hex digit
    g = g.toString(16); // make it a hex digit
    b = b.toString(16); // make it a hex digit
    const color = `#${r}${g}${b}`;
    return color;
}

function copyTouch({ identifier, pageX, pageY }) {
    return { identifier, pageX, pageY };
}

function ongoingTouchIndexById(idToFind) {
    for (let i = 0; i < ongoingTouches.length; i++) {
        const id = ongoingTouches[i].identifier;

        if (id === idToFind) {
            return i;
        }
    }
    return -1;    // not found
}

let resizeObserver = new ResizeObserver(() => {
    document.querySelector("#canvas").width = window.innerWidth
    document.querySelector("#canvas").height = window.innerHeight
});
const body = document.getElementsByTagName("body")[0];

resizeObserver.observe(body);

let observer = new MutationObserver((changes) => {
    if (changes[0].addedNodes.length > 0) {
        document.querySelector(".container-fluid").style.position = "initial"
    } else {
        document.querySelector(".container-fluid").style.position = "relative"
    }
});
